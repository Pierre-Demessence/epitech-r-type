#ifdef __linux__

#include <dirent.h>
#include <cerrno>
#include <cstring>
#include <cstddef>
#include <unistd.h>

#include "UDirectoryLister.hh"
#include "RTException.hh"

DirectoryLister::DirectoryLister()
{}

DirectoryLister::~DirectoryLister()
{}

std::vector<std::string> DirectoryLister::listDirectory(const std::string &directory) const
{
  DIR				*dir;
  struct dirent			*buffer;
  std::vector<std::string>	result;
  int				name_max;

  name_max = pathconf(directory.c_str(), _PC_NAME_MAX);
  if (name_max == -1)
    throw RTException(std::string("Maximum file name size is not defined in directory ")
		      + directory + ": "
		      + std::string(strerror(errno)));

  if (!(dir = opendir(directory.c_str())))
    throw RTException(std::string("Cannot open dir ") + directory + ": "
		      + std::string(strerror(errno)));

  errno = 0;
  while (dir && ((buffer = readdir(dir)) != NULL))
    {
      if (buffer->d_name[0] == '.')
	      continue ;
      result.push_back(buffer->d_name);
    }
  if (errno != 0)
    throw RTException(std::string("Cannot readdir ") + directory + ": "
		      + std::string(strerror(errno)));
  return result;
}

#endif /* !__linux__ */
